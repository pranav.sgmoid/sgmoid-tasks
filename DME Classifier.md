# DME Classifier
## What it does
This model is a part of a larger pipeline to classify Diabetic Retinopathy, DR for short. One step in classifying DR is also to test for Diabetic Macular Edema (DME). This is the DME classifier in the larger pipeline of DR classification. This stage of the pipe
1. first downloads the training data stored in the cloud.
2. Creates and trains the model to classify DME
3. Generates reports bassed on the classifications and the mis-classifications of the model

## Running
`main.py` is the file that needs to be run in order to create and train the model.
This will 
- Download the data
- Create a model
- Train it
- Create a report

The entire run is saved in a folder with a unique string like `'001_s23_dme_'` followed by the datetime it was run. If there is already a saved model using the `inference` options below, only the inference can be run

```
$ python main.py
```
## Configuration options
All of the configuration options for the DME classifier can be found in `config.yaml` file. To comment out any line use `#` sign. The configuration is subsectioned into parts to give each of the options some context.
### Constants
| Option | Purpose | Default | 
| :--: | :--: | :--: |
|`version`| The current version of the classifier | 1 |
| `code` | A special token to indicate where in the pipeline it belongs | s23 |
| `classifier` | name of the model | dme | 

### Setup
The setup options are found under `path`. These specify the directories where the model will recieve inputs from and write outputs to. 
| Option | Purpose | Default | 
| :--: | :--: | :--: |
|`base`| Name of the base directory. Every path begins with this. It is the parent of all future created files and directories | `'.'`, i.e, the current directory |
| `image` | Name of the path where downloaded image data must be saved | `'data'` |
| `model` | Name of the run specific directory where the pickled models and releated objects are stored. | `'models'` | 
| `report` | Name of the run specific directory where the generated report files are stored. | `'report'` |
| `plots` | Name of the run specific *sub-directory* where the generated plots are stored. Found under the reports directory | `'plots'` |
| `viz` | Name of the run specific *sub-directory* where the generated visualization are stored. Found under the reports directory | `'visualization'` |

### Data
These option relate to specifics about the data. They are found under `data`.
| Option | Purpose | Default | 
| :--: | :--: | :--: |
|`credential`| json file with the credentials to access the data | credentials.json, found in `base` directory |
| `sheet_name` | The google sheet in which the data is present | ScreenRad Labels |
| `sheet_no` | Number of the google sheet to look in | 1 | 
| `filename_col` | The column in which the name of the image file is present. | FILE_NAME |
| `target_col` | The column which has the labels | dme_labels |
| `val_size` | Validation size | 0.15 |
| `test_size` | test size | 0.15 |

### Training
Options to train the model; found under `training`
| Option | Purpose | Default | 
| :--: | :--: | :--: |
|`resize`| image resize shape | 256 |
| `batch_size` | batch size | 16 |
| `freeze_epoch` | Number of epochs to train the model when frozen. | 50 | 
| `unfreeze_epoch` | Number of epochs to train the entire model. | 50 | 
| `model_name` | The model architecture to download for training | `'ResNet50'` |

### Report
Options to customize the generated classification report; found under `report`
| Option | Default | 
| :--:  | :--: |
|`title` | Validation Report |
| `author`  | Subhan De |
| `model_purpose` | S23 DME classifier | 

## Inference
These options serve to load model during inference. They are found under `inference`
| Option | Purpose | Default | 
| :--: | :--: | :--: |
|`run_only_inference`| Training will be skipped | false |
| `i_data` | Needed to load the model |  |
| `i_data_time` | Needed to load the model |  | 

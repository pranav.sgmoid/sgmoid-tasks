from urllib.parse import urlparse

import json

def handler(request):
    """Responds to any HTTP request.
    Args:
        request (flask.Request): HTTP request object.
    Returns:
        The response text or any set of values that can be turned into a
        Response object using
        `make_response <http://flask.pocoo.org/docs/1.0/api/#flask.Flask.make_response>`.
    """
    request_json = request.get_json()
    if request.args and 'url' in request.args:
        return urlparse(request.args['url']).netloc
    elif request_json and 'url' in request_json:
        return urlparse(request_json['url']).netloc
    else:
        return json.dumps({
            'status_code': 404,
        })

